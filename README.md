# Assignment Description
The main idea is to build a REST API recreating some aspects of the Rubrix server, such as textual search or dataset metrics computation.



As the preferred tech stack, we propose FastAPI for the API implementation and Elasticsearch as the main backend engine, but we're open to a different stack. Please, feel free to use your own API framework and/or backend engine which you feel more comfortable.



### The features to implement are:

- Add new data (feel free to define the data model that better fits your solution)
- Query data based on a text field
- Expose one textual metric (the following are some examples but could be another one):
Text length distribution
Token length distribution
Relevant words

# Stack
- docker
- python3
- flask
- elasticsearch

# Python libraries
- flask
- pytest
- elasticsearch

# Dependencies to run the project
- [docker](https://docs.docker.com/engine/install/)
- [docker-compose](https://docs.docker.com/compose/install/linux/)


# Instructions to run the project
- cd root/to/project
- docker-compose up -d --build


# How to use the API
- The first endpoint will ingest the data into a Document, and will return a DocumentId.
- Make sure to check the sample/sample_data.json file which have many examples of the data structure.
```sh
$ curl -d '<json_data>' -H "Content-Type: application/json" -X POST http://0.0.0.0:8000/add
```

- The second endpoint will search a text term in any of the documents and will return a list of documents that matched the textual search, otherwise it will return an empty response with status code 404.
```sh
$ curl http://0.0.0.0:8000/search?text_term=word_or_substring -H "Content-Type: application/json"
```

# Bulk data into elasticsearch
- There is a file located at sample/sample_data.json that can be bulk into an index of the elasticsearch, this is useful and save a lot of time.
```sh
$ curl -XPOST localhost:9200/_bulk -H "Content-type: application/json" --data-binary @/root/to/project/sample/sample_data.json
```

# Postman collection
At the root of the project there is a postman_collection folder with a demo of the API, please use it after deploying all the components as explained above.

# Unit testing
I also created a few test of the utils module functions and the main module functions.

- To run the test please use the command below:
```sh
$ docker-compose exec web python -m pytest test/*
```