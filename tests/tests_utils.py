import unittest

from app.utils import valid_params


class TestUtils(unittest.TestCase):
    def test_valid_params_ok(self):
        params = {"id": 1, "title": "This is a title", "abstract": "bhjlfdshgiuvsgihul"}
        result = valid_params(params)
        self.assertTrue(result)

    def test_valid_params_ko(self):
        params = {"ids": 1, "abstract": "bhjlfdshgiuvsgihul"}
        result = valid_params(params)
        self.assertFalse(result)

    def test_valid_params_empty(self):
        params = {}
        result = valid_params(params)
        self.assertFalse(result)

