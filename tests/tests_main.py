import unittest

from elasticsearch import Elasticsearch

from app.main import is_duplicated_ingestion


class TestMain(unittest.TestCase):
    def setUp(self) -> None:
        self.es = Elasticsearch("elasticsearch:9200", verify_certs=False)
        self.index = "test"

    def test_is_duplicated_ingestion_false(self):
        params = {"id": 1, "title": "This is a title", "abstract": "bhjlfdshgiuvsgihul"}
        result = is_duplicated_ingestion(params)
        self.assertFalse(result)

