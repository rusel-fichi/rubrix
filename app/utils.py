"""
    This module contains common functions that will be use on the project
"""


def valid_params(params):
    if params.get("id") and params.get("title") and params.get("abstract"):
        return True
    return False
