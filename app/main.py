import logging

from elasticsearch import Elasticsearch, NotFoundError
from flask import Flask, Response, request, jsonify, make_response

from app.utils import valid_params

app = Flask(__name__)
es = Elasticsearch("elasticsearch:9200", verify_certs=False)
index = "sample"


def is_duplicated_ingestion(data: dict):
    """
    Returns True if there is a matching document
    :param data: data dict
    :return:
    """
    res = es.search(index=index, body={"query": {"match": {"id": data.get("id")}}})
    if res["hits"]["hits"]:
        return True
    return False


@app.route("/add", methods=['POST'])
def add_data():
    """
    Add data to a Document
    :return: Response
    """
    # get all params from request
    params = request.get_json()
    if not valid_params(params) or is_duplicated_ingestion(params):
        logging.error(f"There was an error with the data sent to the server")
        return Response("", status=400)
    try:
        res = es.index(index=index, body=params)
    except Exception as ex:
        logging.error(f"There was an error in the server, error: {ex}")
        return Response("", status=500)

    return Response(res["_id"], status=201)


@app.route("/search", methods=['GET'])
def search_text() -> Response:
    """
    Search text term in a document
    :return: Response
    """
    params = request.args
    text_term = params.get("text_term")

    if not text_term:
        logging.error(f"The path parameter <text_term> was not sent in the request")
        return Response("", status=400)

    res = es.search(index="sample", size=100, body={"query": {
        "query_string": {"fields": ["title", "abstract"], "query": f"*{text_term}*"}
    }})

    data = [{**item["_source"], **{"_id": item["_id"]}} for item in res["hits"]["hits"]]
    if data:
        response = make_response(jsonify({"results": data, "hits": len(data)}), 201)
        response.headers["Content-Type"] = "application/json"
        return response
    else:
        return Response("", status=404)


@app.route("/delete/<string:document_id>", methods=['DELETE'])
def delete_document(document_id: str) -> Response:
    """
    Delete Document id exist
    :param document_id: Document id
    :return: Response
    """
    try:
        es.delete(index=index, id=document_id)
    except NotFoundError as ex:
        logging.error(f"The document_id: {document_id} does not exist")
        return Response("", status=404)
    return Response("", status=204)

